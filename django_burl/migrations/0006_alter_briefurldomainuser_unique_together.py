# Generated by Django 4.0.2 on 2022-02-25 17:47

from django.conf import settings
from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('sites', '0002_alter_domain_unique'),
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('django_burl', '0005_briefurldomainuser'),
    ]

    operations = [
        migrations.AlterUniqueTogether(
            name='briefurldomainuser',
            unique_together={('site', 'user')},
        ),
    ]
