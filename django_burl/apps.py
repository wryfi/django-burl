from django.apps import AppConfig


class DjangoBurlConfig(AppConfig):
    name = "django_burl"
    verbose_name = "brief urls"
